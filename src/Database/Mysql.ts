import * as mysql from 'mysql';
import Rollbar from './../Rollbar/Rollbar';

class Mysql {
    public static database: mysql.Connection;

    public static boot() {
        this.database = mysql.createConnection({
            host: '139.59.41.195',
            user: 'alphaman',
            password: 'passwordROFL',
            database: 'miroServices'
        });
        this.database.connect((err: mysql.MysqlError) => {
            if (err) {
                Rollbar.critical(err);
            }
        });
    }
}

export default Mysql;
