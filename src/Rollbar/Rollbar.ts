import * as RollbarInstance from 'rollbar';

const Rollbar: RollbarInstance = new RollbarInstance({
    accessToken: 'be38b3e730984500a7a6d40c588bfe3f',
    captureUncaught: true,
    captureUnhandledRejections: true,
});

export default Rollbar;
