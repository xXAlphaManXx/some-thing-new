import {join} from 'path';
import * as express from 'express';
import * as bodyParser from 'body-parser';
import * as RedisStore from 'connect-redis';
import * as cookieParser from 'cookie-parser';
import * as expressSession from 'express-session';

import Web from './Routes/Web';
import Rollbar from './../Rollbar/Rollbar';

class App {
    private app: express.Application;
    private router: express.Router;

    public Bootstrap(): void {
        this.app = express();
        this.router = express.Router();

        this.registerMiddlewares();
        this.registerRoutes();
        this.listenForSomeAdventure();
    }

    private registerMiddlewares(): void {
        let redisStore: RedisStore.RedisStore = RedisStore(expressSession);
        let sessionConfig: expressSession.SessionOptions = {
            secret: 'eO404&98Fij7z96574MsAO330E535WfG',
            store: new redisStore({
                host: 'redis-18992.c17.us-east-1-4.ec2.cloud.redislabs.com',
                port: 18992,
                url: '//redis-18992.c17.us-east-1-4.ec2.cloud.redislabs.com:18992',
                pass: 'qJuPcgKj7XUFxe3sA1q6EvVqt83FTDX6',
                ttl: 3600,
            }),
            resave: false,
            saveUninitialized: true,
            cookie: {
                secure: false
            }
        };

        if(this.app.get('env') == 'production') {
            this.app.set('trust proxy', 1);
            sessionConfig.cookie.secure = true;
        }

        this.app.use((req: express.Request, res: express.Response, next: express.NextFunction) => {
            res.locals.BASE_URL = "http://localhost:8080";
            console.log('[CONNECTED] -> An user requested', req.path, 'via the', req.method, 'method');

            next();
        });
        this.app.use(bodyParser.urlencoded({
            extended: false
        }));
        this.app.use(cookieParser('eO404&98Fij7z96574MsAO330E535WfG'));
        this.app.use(expressSession(sessionConfig));
        this.app.use((req: express.Request, res: express.Response, next: express.NextFunction) => {
            if (typeof req.session.guest == 'boolean') {
                console.log('LOGGED IN');
                res.locals.isGuest = false;

                return next();
            }

            res.locals.isGuest = true;
            next();
        });

        this.app.set('view engine', 'pug');
        this.app.set('views', join(__dirname, '../..', 'src/Resources/Views'));

        this.app.use(Rollbar.errorHandler());
    }

    private registerRoutes(): void {
        this.app.use(
            Web.listRoutes(this.router)
        );
    }

    private listenForSomeAdventure(): void {
        this.app.listen(8080, () => {
            console.log('Listening on PORT 8080');
        });
    }
}

export default App;
