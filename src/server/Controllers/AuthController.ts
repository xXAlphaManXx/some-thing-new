import { createHash } from 'crypto';
import { Request, Response } from 'express';
import { MysqlError, FieldInfo, Query } from 'mysql';

import Mysql from './../../Database/Mysql';
import Rollbar from '../../Rollbar/Rollbar';

class AuthController {
    public static showLogin(req: Request, res: Response): void {
        if((typeof req.session.guest == 'boolean') && !req.session.guest) {
            return res.redirect('http://localhost:8080/user/dashboard');
        }

        return res.render('Login');
    }

    public static showRegister(req: Request, res: Response): void {
        if((typeof req.session.guest == 'boolean') && !req.session.guest) {
            return res.redirect('http://localhost:8080/user/dashboard');
        }

        return res.render('Register');
    }

    public static processLogin(req: Request, res: Response): void | Query | Response {
        if((typeof req.session.guest == 'boolean') && !req.session.guest) {
            return res.redirect('http://localhost:8080/user/dashboard');
        }

        const email: string = req.body.email;
        const unsecurePass: string = req.body.password;

        if (!email || !unsecurePass) {
            return res.status(403).send('Fill all the form fields');
        }

        const encryptedPass: string = createHash('md5').update(unsecurePass).digest('hex');

        Mysql.boot();
        return Mysql.database.query('SELECT * FROM users WHERE email = ? AND password = ?', [
            email, encryptedPass
        ], (err: MysqlError, loginResult: any, fields: FieldInfo[]) => {
            if (err) {
                Rollbar.critical(err);
                return res.status(500).send('<h1>OOPS. SOMETHING BAD HAPPENED. REPORT IT TO ADMINS</h1>');
            }

            let loginResultOBJ: Object = JSON.parse(JSON.stringify(loginResult));

            if(Object(loginResultOBJ).length > 0) {
                req.session.guest = false;
                req.session.user_id = loginResultOBJ[0].id;
                req.session.email = loginResultOBJ[0].email;

                return res.send('Login Successfull');
            }

            return res.status(403).send('Credentails do not match');
        });
    }

    public static processRegistration(req: Request, res: Response): void | Query | Response {
        if((typeof req.session.guest == 'boolean') && !req.session.guest) {
            return res.redirect('http://localhost:8080/user/dashboard');
        }

        const email: string = req.body.email;
        const password: string = req.body.password;
        const rpassword: string = req.body.rpassword;

        if (password != rpassword && !email) {
            return res.status(403).send('Fill all the form fields correctly');
        }

        Mysql.boot();
        const encryptedPass: string = createHash('md5').update(password).digest('hex');

        return Mysql.database.query('SELECT * FROM users WHERE email = ?', [
            email
        ], (err: MysqlError, checkAccount: any, fields: FieldInfo[]) => {
            if (err) {
                Rollbar.critical(err);
                return res.status(500).send('<h1>OOPS. SOMETHING BAD HAPPENED. REPORT IT TO ADMINS</h1>');
            }

            const checkAccountOBJ: Object = JSON.parse(JSON.stringify(checkAccount));

            if(Object(checkAccountOBJ).length > 0) {
                return res.status(403).send('Email already exists');
            }

            return Mysql.database.query('INSERT INTO users (email, password) VALUES (?, ?)', [
                email, encryptedPass
            ], (err: MysqlError, registerResult: any, fields: FieldInfo[]) => {
                if (err) {
                    Rollbar.critical(err);
                    return res.status(500).send('<h1>OOPS. SOMETHING BAD HAPPENED. REPORT IT TO ADMINS</h1>');
                }

                const registerResultOBJ = JSON.parse(JSON.stringify(registerResult));
                
                if(registerResultOBJ.affectedRows == 1) {
                    return Mysql.database.query('SELECT * FROM users WHERE email = ? AND password = ?', [
                        email, encryptedPass
                    ], (err: MysqlError, loginResult: any, fields: FieldInfo[]) => {
                        if (err) {
                            Rollbar.critical(err);
                            return res.status(500).send('<h1>OOPS. SOMETHING BAD HAPPENED. REPORT IT TO ADMINS</h1>');
                        }
            
                        let loginResultOBJ: Object = JSON.parse(JSON.stringify(loginResult));
            
                        if(Object(loginResultOBJ).length > 0) {
                            req.session.guest = false;
                            req.session.user_id = loginResultOBJ[0].id;
                            req.session.email = loginResultOBJ[0].email;
            
                            return res.send('Login Successfull');
                        }
            
                        return res.status(403).send('Credentails do not match');
                    });
                }

                return res.send('SOMETHING BAD HAPPENED. TELL ADMIN THE CODE 1');
            });
        });
    }

    public static logout(req: Request, res: Response): void {
        delete req.session.guest;
        delete req.session.user_id;
        delete req.session.email;

        return res.redirect('http://localhost:8080/');
    }
}

export default AuthController;
