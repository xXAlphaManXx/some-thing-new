import * as randomatic from 'randomatic';
import { Request, Response, Router } from 'express';
import { Query, MysqlError, FieldInfo } from 'mysql';

import Mysql from '../../Database/Mysql';
import Rollbar from '../../Rollbar/Rollbar';

class LinkController {
    public static showCreateLink(req: Request, res: Response): void {
        if((typeof req.session.guest == 'undefined')) {
            return res.redirect('http://localhost:8080/auth/register');
        }

        return res.render('Create');
    }

    public static createLink(req: Request, res: Response): void | Query {
        if((typeof req.session.guest == 'undefined')) {
            return res.redirect('http://localhost:8080/auth/register');
        }

        const refer_id: string = randomatic('aA0', 5);

        Mysql.boot();
        return Mysql.database.query('INSERT into `links_details` (`refer_id`,`user_id`, `title`, `link_id`, `get_link`,`views`, `action`, `completed_stu`, `deleted`) VALUES (?, ?, ?, ?, ?, 0, 0, 0, 0)', [
            refer_id, req.session.user_id, req.body.Title, req.body.link_id, req.body.get_link
        ], (err: MysqlError, result: any, fields: FieldInfo[]) => {
            if (err) {
                Rollbar.critical(err);
                return res.status(500).send('<h1>OOPS. SOMETHING BAD HAPPENED. REPORT IT TO ADMINS</h1>');
            }

            const resultsOBJ = JSON.parse(JSON.stringify(result));

            if (typeof req.body.title_link == 'object') {
                let i: number = 0;
                req.body.title_link.forEach(element => {
                    console.log('OKAY', i);
                    Mysql.database.query('INSERT into `all_links` (`insert_id`, `link`, `link_opt`, `media`, `deleted`) VALUES (?, ?, ?, ?, 0)', [
                        resultsOBJ.insertId, element, req.body.title_link_opt[i], req.body.media[i]
                    ], (err: MysqlError, linkCreated: any, fields: FieldInfo[]) => {
                        if (err) {
                            Rollbar.critical(err);
                            return res.status(500).send('<h1>OOPS. SOMETHING BAD HAPPENED. REPORT IT TO ADMINS</h1>');
                        }
                        console.log('DONE', i);
                        i++;
                    });
                });

                return res.status(200).send(refer_id);
            }

            return Mysql.database.query('INSERT into `all_links` (`insert_id`, `link`, `link_opt`, `media`, `deleted`) VALUES (?, ?, ?, ?, 0)', [
                resultsOBJ.insertId, req.body.title_link, req.body.title_link_opt, req.body.media
            ], (err: MysqlError, linkCreated: any, fields: FieldInfo[]) => {
                if (err) {
                    Rollbar.critical(err);
                    return res.status(500).send('<h1>OOPS. SOMETHING BAD HAPPENED. REPORT IT TO ADMINS</h1>');
                }

                return res.status(200).send(refer_id);
            });
        });
    }

    public static showLink(req: Request, res: Response): void | Query {
        Mysql.boot();
        return Mysql.database.query('SELECT * FROM links_details WHERE refer_id = ?', [
            req.params.linkID
        ], (err: MysqlError, result: any, fields: FieldInfo[]) => {
            const completeLinkOBJ = [];
            const resultOBJ = JSON.parse(JSON.stringify(result));

            if (resultOBJ.length > 0) {
                completeLinkOBJ.push(resultOBJ[0])
                return Mysql.database.query('SELECT * FROM all_links WHERE insert_id = ?', [
                    completeLinkOBJ[0].id
                ], (err: MysqlError, linkDetails: any, fields: FieldInfo[]) => {
                    if (err) {
                        Rollbar.critical(err);
                        return res.status(500).send('<h1>OOPS. SOMETHING BAD HAPPENED. REPORT IT TO ADMINS</h1>');
                    }

                    const linkDetailsOBJ = JSON.parse(JSON.stringify(linkDetails))
                    completeLinkOBJ.push(linkDetailsOBJ)
                    
                    res.render('Link',{
                        link: completeLinkOBJ[0],
                        details: completeLinkOBJ[1]
                    });

                    Mysql.database.query('UPDATE links_details SET views = views + 1 WHERE id = ?', [
                        completeLinkOBJ[0].id
                    ], (err: MysqlError, updatedCount: any, fields: FieldInfo[]) => {
                        if (err) {
                            Rollbar.critical(err);
                        }
                    });
                });            
            }

            return res.redirect('http://localhost:8080/');
        });
    }

    public static getLink(req: Request, res: Response): void | Query {
        if (req.params.link) {
            Mysql.boot();
            return Mysql.database.query('SELECT link_id FROM links_details WHERE id = ?', [
                req.params.link
            ], (err: MysqlError, results: any, fields: FieldInfo[]) => {
                if (err) {
                    Rollbar.critical(err);
                    return res.status(500).send('<h1>OOPS. SOMETHING BAD HAPPENED. REPORT IT TO ADMINS</h1>');
                }

                if(results.length > 0) {
                    return res.render('Getit', {
                        link: results[0].link_id
                    });
                }

                return res.redirect('http://localhost:8080/');
            });
        }

        return res.redirect('http://localhost:8080/');
    }

    public static countLink(req: Request, res: Response): void | Query {
        if(req.body.final_count) {
            Mysql.boot();
            return Mysql.database.query('UPDATE links_details SET completed_stu = completed_stu + 1 WHERE id = ?', [
                req.body.id
            ], (err: MysqlError, results: any, fields: FieldInfo[]) => {
                if (err) {
                    Rollbar.critical(err);
                    return res.status(500).send('<h1>OOPS. SOMETHING BAD HAPPENED. REPORT IT TO ADMINS</h1>');
                }

                return res.send('Success');
            });
        }
    
        if(req.body.action) {
            Mysql.boot();
            return Mysql.database.query('UPDATE links_details SET action = action + 1 WHERE id = ?', [
                req.body.id
            ], (err: MysqlError, results: any, fields: FieldInfo[]) =>{
                if (err) {
                    Rollbar.critical(err);
                    return res.status(500).send('<h1>OOPS. SOMETHING BAD HAPPENED. REPORT IT TO ADMINS</h1>');
                }

                return res.send('Success');
            });
        }
    }

    public static deleteLink(req: Request, res: Response): Query | void {
        if((typeof req.session.guest == 'undefined')) {
            return res.redirect('http://localhost:8080/auth/register');
        }

        Mysql.boot();
        return Mysql.database.query('UPDATE links_details SET deleted = 1 WHERE user_id = ? AND refer_id = ?', [
            req.session.user_id, req.body.refer
        ], (err: MysqlError, resukt: any, fields: FieldInfo[]) => {
            return res.status(200)
                .send('Deleted');
        });
    }
}

export default LinkController;
