import { join } from 'path';
import { readFile } from 'fs';
import { Request, Response, Router } from 'express';
import { Query, MysqlError, FieldInfo } from 'mysql';

import Mysql from '../../Database/Mysql';
import Rollbar from '../../Rollbar/Rollbar';

class HomeController {
    public static index(req: Request, res: Response): void {
        return readFile(
            join(__dirname, '../../..', 'src/Resources/Views/Main.html'), {
                encoding: 'utf-8'
            }, (err: NodeJS.ErrnoException, data: string) => {
                return res.send(data);
            }
        );
    }

    public static dashboard(req: Request, res: Response): void | Query {
        if((typeof req.session.guest == 'undefined')) {
            return res.redirect('http://localhost:8080/auth/register');
        }

        Mysql.boot();
        return Mysql.database.query('SELECT SUM(completed_stu) AS cstu_sum, SUM(action) AS action_sum, SUM(views) AS view_sum FROM links_details where user_id = ? AND deleted = 0', [
            req.session.user_id
        ], (err: MysqlError, results: any, fields: FieldInfo[]) => {
            if (err) {
                Rollbar.critical(err);
                return res.send('<h1>OOPS. SOMETHING BAD HAPPENED. REPORT IT TO ADMINS</h1>');
            }

            const resultOBJ = JSON.parse(JSON.stringify(results));

            let s2uAttempt: number = 0;
            let actionSum: number = 0;
            let viewSum: number = 0;

            if (resultOBJ[0].cstu_sum) {
                s2uAttempt = resultOBJ[0].cstu_sum;
            } else {
                s2uAttempt = 0;
            }

            if (resultOBJ[0].action_sum) {
                actionSum = resultOBJ[0].action_sum;
            } else {
                actionSum = 0;
            }

            if (resultOBJ[0].view_sum) {
                viewSum = resultOBJ[0].view_sum;
            } else {
                viewSum = 0;
            }

            return Mysql.database.query('SELECT * FROM links_details WHERE user_id= ? AND deleted = 0 ORDER BY id DESC', [
                req.session.user_id
            ], (err: MysqlError, results: any, fields: FieldInfo[]) => {
                if (err) {
                    Rollbar.critical(err);
                    return res.send('<h1>OOPS. SOMETHING BAD HAPPENED. REPORT IT TO ADMINS</h1>');
                }

                const linksObj = JSON.parse(JSON.stringify(results));
                
                return res.render('Dashboard', {
                    s2u_attempt: s2uAttempt,
                    view_sum: viewSum,
                    action_sum: actionSum,
                    links: linksObj
                });
            });
        });
    }
}

export default HomeController;
