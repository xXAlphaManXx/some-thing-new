import {Request, Response, Router} from 'express';
import HomeController from './../Controllers/HomeController';
import AuthController from './../Controllers/AuthController';
import LinkController from './../Controllers/LinkController';

class Web {
    private static router: Router;

    public static listRoutes(router: Router): Router {
        this.router = router
        this.index();
        this.loginLogic();
        this.registerLogic();
        this.logout();
        this.dashboard();
        this.createLink();
        this.showLink();
        this.deleteLink();

        return this.router;
    }

    private static index(): Router {
        return this.router.get('/', HomeController.index);
    }

    private static loginLogic(): Router {
        this.router.get('/auth/login', AuthController.showLogin);
        this.router.post('/auth/login', AuthController.processLogin);

        return this.router;
    }

    private static registerLogic(): Router {
        this.router.get('/auth/register', AuthController.showRegister);
        this.router.post('/auth/register', AuthController.processRegistration);

        return this.router;
    }

    private static logout(): Router {
        return this.router.get('/auth/logout', AuthController.logout);
    }

    private static dashboard(): Router {
        return this.router.get('/user/dashboard', HomeController.dashboard);
    }

    private static createLink(): Router {
        this.router.get('/link/create', LinkController.showCreateLink);
        this.router.post('/link/create', LinkController.createLink);

        return this.router;
    }

    private static showLink(): Router {
        this.router.get('/:linkID', LinkController.showLink);
        this.router.get('/links.php/:linkID', LinkController.showLink);
        this.router.get('/link/get/:link', LinkController.getLink);
        this.router.post('/link/count', LinkController.countLink);

        return this.router;
    }

    private static deleteLink(): Router {
        return this.router.delete('/link', LinkController.deleteLink);
    }
}

export default Web;
